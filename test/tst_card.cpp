#include <catch.hpp>

#include "card.h"

TEST_CASE("Default value")
{
    Card card;

    REQUIRE(card.rank() == "2");
    REQUIRE(card.suit() == Card::Suit::Clubs);
    REQUIRE(card.value() == 2);
    REQUIRE(card.isAce() == false);
}

TEST_CASE("Large value")
{
    Card card("10");

    REQUIRE(card.rank() == "10");
    REQUIRE(card.suit() == Card::Suit::Clubs);
    REQUIRE(card.value() == 10);
    REQUIRE(card.isAce() == false);
}

TEST_CASE("Face card")
{
    Card card("K");

    REQUIRE(card.rank() == "K");
    REQUIRE(card.suit() == Card::Suit::Clubs);
    REQUIRE(card.value() == 10);
    REQUIRE(card.isAce() == false);
}

TEST_CASE("Ace")
{
    Card card("A");

    REQUIRE(card.rank() == "A");
    REQUIRE(card.suit() == Card::Suit::Clubs);
    REQUIRE(card.value() == 1);
    REQUIRE(card.isAce() == true);
}

TEST_CASE("Invalid rank")
{
    Card card("Z");

    REQUIRE(card.rank() == "Z");
    REQUIRE(card.suit() == Card::Suit::Clubs);
    REQUIRE(card.value() == 0);
    REQUIRE(card.isAce() == false);
}
