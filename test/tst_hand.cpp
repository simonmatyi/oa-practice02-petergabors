#include <catch.hpp>

#include "hand.h"

TEST_CASE("Passing cards")
{
    Hand hand;

    SECTION("Empty handed")
    {
        REQUIRE(hand.cards().size() == 0);
        REQUIRE_FALSE(hand.hasCard());
    }

    SECTION("Take a card")
    {
        hand.takeCard(Card());
        REQUIRE(hand.cards().size() == 1);
        REQUIRE(hand.hasCard());
    }

    SECTION("Give card")
    {
        hand.takeCard(Card());
        REQUIRE(hand.hasCard());

        REQUIRE(hand.giveTopCard() == Card());
        REQUIRE(hand.cards().size() == 0);
        REQUIRE_FALSE(hand.hasCard());
    }

    SECTION("Give a card when has none")
    {
        REQUIRE_FALSE(hand.hasCard());
        REQUIRE_FALSE(hand.giveTopCard().isValid());
        REQUIRE_FALSE(hand.hasCard());
    }
}

TEST_CASE("Determining the value of the hand")
{
    Hand hand;

    SECTION("Taking a 2")
    {
        hand.takeCard(Card("2"));
        REQUIRE(hand.value() == 2);
    }

    SECTION("Taking 2 twice")
    {
        hand.takeCard(Card("2"));
        hand.takeCard(Card("2"));
        REQUIRE(hand.value() == 4);
    }

    SECTION("Single ace")
    {
        hand.takeCard(Card("A"));
        REQUIRE(hand.value() == 11);
    }

    SECTION("Two queens and an ace")
    {
        hand.takeCard(Card("Q"));
        hand.takeCard(Card("Q"));
        hand.takeCard(Card("A"));
        REQUIRE(hand.value() == 21);
    }

    SECTION("Two queens and an two aces")
    {
        hand.takeCard(Card("Q"));
        hand.takeCard(Card("Q"));
        hand.takeCard(Card("A"));
        hand.takeCard(Card("A"));
        REQUIRE(hand.value() == 22);
    }
}

TEST_CASE("Determining other aspects of the hand")
{
    Hand hand;

    SECTION("Has less 21 with non-ace cards")
    {
        hand.takeCard(Card("J"));
        hand.takeCard(Card("9"));
        hand.takeCard(Card("2"));
        REQUIRE_FALSE(hand.hasAce());
        REQUIRE_FALSE(hand.hasBlackjack());
        REQUIRE_FALSE(hand.busts());
        REQUIRE_FALSE(hand.canSplit());
    }

    SECTION("Has less 22 with non-ace cards")
    {
        hand.takeCard(Card("J"));
        hand.takeCard(Card("9"));
        hand.takeCard(Card("3"));
        REQUIRE_FALSE(hand.hasAce());
        REQUIRE_FALSE(hand.hasBlackjack());
        REQUIRE(hand.busts());
        REQUIRE_FALSE(hand.canSplit());
    }

    SECTION("Has 21 with an ace")
    {
        hand.takeCard(Card("A"));
        hand.takeCard(Card("4"));
        hand.takeCard(Card("6"));
        REQUIRE(hand.hasAce());
        REQUIRE_FALSE(hand.hasBlackjack());
        REQUIRE_FALSE(hand.busts());
        REQUIRE_FALSE(hand.canSplit());
    }

    SECTION("Has 21 with an ace and a ten")
    {
        hand.takeCard(Card("A"));
        hand.takeCard(Card("10"));
        REQUIRE(hand.hasAce());
        REQUIRE(hand.hasBlackjack());
        REQUIRE_FALSE(hand.busts());
        REQUIRE_FALSE(hand.canSplit());
    }

    SECTION("Has two cards with the same value")
    {
        hand.takeCard(Card("5"));
        hand.takeCard(Card("5"));
        REQUIRE_FALSE(hand.hasAce());
        REQUIRE_FALSE(hand.hasBlackjack());
        REQUIRE_FALSE(hand.busts());
        REQUIRE(hand.canSplit());
    }
}
