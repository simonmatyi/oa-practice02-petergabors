#include "hand.h"

#include <algorithm>

Hand::Hand()
{

}

const QList<Card> &Hand::cards() const
{
    return _cards;
}

void Hand::takeCard(const Card &card)
{
    _cards.append(card);
}

Card Hand::giveTopCard(void)
{
    Card card("0");

    if(hasCard())
    {
        card = _cards.takeFirst();
    }
    return card;
}

int Hand::value() const
{
    auto sortedCards = _cards;
    std::sort(sortedCards.rbegin(), sortedCards.rend());

    return std::accumulate( sortedCards.constBegin(), sortedCards.constEnd(), 0,
                            [](int sum, const Card &card) {
                                if(card.isAce() && (sum <= (BustLimit - 11)))
                                {
                                    return sum + 11;
                                }
                                else
                                {
                                    return sum + card.value();
                                }
                            } );
}

bool Hand::hasCard() const
{
    return _cards.size() > 0;
}

bool Hand::hasAce() const
{
    return std::any_of(_cards.constBegin(), _cards.constEnd(),
                       [](const Card &card) { return card.isAce(); } );
}

bool Hand::hasBlackjack() const
{
    return hasAce() && (_cards.size() == 2);
}

bool Hand::busts() const
{
    return value() > BustLimit;
}

bool Hand::canSplit() const
{
    bool result = false;

    if(_cards.size() == 2)
    {
        result = _cards.at(0).value() == _cards.at(1).value();
    }

    return result;
}
