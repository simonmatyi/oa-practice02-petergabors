#include "card.h"

Card::Card(QString rank, Suit suit):
    _rank(rank),
    _suit(suit)
{
}

Card &Card::operator=(const Card &rhs)
{
    _rank = rhs._rank;
    _suit = rhs._suit;
    return *this;
}

bool Card::operator==(const Card &rhs) const
{
    return _rank == rhs._rank
        && _suit == rhs._suit;
}

bool Card::operator<(const Card &rhs) const
{
    return value() < rhs.value();
}

QString Card::rank() const
{
    return _rank;
}

Card::Suit Card::suit() const
{
    return _suit;
}

int Card::value() const
{
    return Ranks.value(_rank);
}

bool Card::isAce() const
{
    return _rank == "A";
}

bool Card::isValid() const
{
    return value() > 0;
}
