#ifndef CARD_H
#define CARD_H

#include <QMap>
#include <QString>

class Card
{
public:
    enum class Suit{
        Clubs,
        Spades,
        Diamonds,
        Hearts
    };

    const QMap<QString, int> Ranks = {
        {"A",  1 },
        {"2",  2 },
        {"3",  3 },
        {"4",  4 },
        {"5",  5 },
        {"6",  6 },
        {"7",  7 },
        {"8",  8 },
        {"9",  9 },
        {"10", 10},
        {"J",  10},
        {"Q",  10},
        {"K",  10},
    };

    Card(QString rank = "2", Suit suit = Suit::Clubs);

    Card &operator=(const Card &rhs);
    bool operator==(const Card &rhs) const;
    bool operator<(const Card &rhs) const;

    QString rank() const;
    Suit suit() const;
    int value() const;
    bool isAce() const;
    bool isValid() const;

private:
    QString _rank;
    Suit _suit;
};

#endif // CARD_H
