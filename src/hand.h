#ifndef HAND_H
#define HAND_H

#include <QList>

#include "card.h"

class Hand
{
public:
    static const int BustLimit = 21;

    Hand();

    const QList<Card> &cards() const;
    void takeCard(const Card &card);
    /* If there are no cards, gives an invalid one with 0 rank */
    Card giveTopCard(void);

    /* The function determines the maximum value without bust if possible */
    int value() const;

    bool hasCard() const;
    bool hasAce() const;
    bool hasBlackjack() const;
    bool busts() const;
    bool canSplit() const;

private:
    QList<Card> _cards;
};

#endif // HAND_H
